---
layout: post
title: "Auto.JS基础"
date: 2020-11-23
description: "安卓、Autojs、测试"
tag: 安卓、Autojs、测试
---

## Auto.JS

### Auto.JS简介

> 根据官方文档的定义，Auto.js是一款**无需root**权限的JavaScript自动化软件，顾名思义是用**JavaScript**语言来写的。
>
> 首先Auto.JS是一款**安卓端**的自动化软件，它可以让安卓端手机通过**无障碍**就可以在**无root**的情况下实现自动化操作，同时也支持将脚本**打包**成APK文件。
>
> 目前的话Auto.js免费版的已经停止维护了，收费版的在某宝可购买永久使用权，但个人认为免费版的已经完全够学习使用了。

### 相关文档

* [官方文档](https://hyb1996.github.io/AutoJs-Docs) （但由于文档是在GitHub上的，然后国内会屏蔽一些外网，导致一些GItHub pages页面无法访问或访问过慢，下面会有种解决方法）
* [GitHub项目](https://github.com/hyb1996/Auto.js)
* [别人抄录修改的文档](https://easydoc.xyz/doc/25791054/uw2FUUiw/3bEzXb4y)（觉得解决上面问题麻烦的可以直接看这个）
* [Auto.JS文件](http://file.fxhaoo.cn/AutoJs-4.1.1a.Alpha2.apk?e=1605846024&token=wB_cehH4vdiXeXVHKtS_L-zhM57WTdaXaHvHwZMZ:VO6Okl6oIrZjmL45LnHMvIS8GvU=)
* [论坛](https://www.autojs.org/)（已停止维护，但里面有些内容还是可以借鉴学习的）

### 解决GitHub访问过慢或无法访问的问题

* 原因好像是由于GitHub域名解析失败导致的，我们通过修改host文件来解决。我们访问网址时是先搜索host文件，如果文件里有对应的ip就不需要进行DNS域名解析，因此可以将需要访问的GitHub网址ip配成静态IP，这样就可以减少解析的过程，提高访问的速度。

* [HOST文件](C:\Windows\System32\drivers\etc)  [C:\Windows\System32\drivers\etc\HOSTS]

* 步骤：

  * 首先打开 [DNS查询网站](http://tool.chinaz.com/dns/)

  * 输入需要查询的域名，解析出该域名的IP

    ![image-20201120134814309](http://cdn.fxhaoo.cn/image-20201120134814309.png)

  * 然后打开HSOT文件，在最下方添加添加上对应的IP(其中一条即可)和域名，然后就。。应该解决了无法访问的问题了。。

    ![image-20201120135414328](http://cdn.fxhaoo.cn/image-20201120135414328.png)

### Auto.js与Airtest对比

回归正题~

* 对于平台
  * 相对于Airtest，Auto.JS就比较局限，Airtest基本上可以说是支持全平台了，而AutoJS就只支持安卓端
* 对于控件查找
  * Airtest支持的更加全面且更加方便（图色加节点），AutoJS则支持节点查找元素，图色查找好像比较麻烦(暂未使用过)
* 对于开发语言
  * Airtest采用的python，开发效率较高，扩展性较强，AutoJS采用的JavaScript语言进行开发的
* 这么看起来AutoJS应该是被Airtest吊打才对，但Autojs执行速度快哈，封装成APK也很方便，这就意味着在脱离PC端的情况下，也可以运行。

### Autojs的使用

#### IDE

* **vscode**：对于微软的这款IDE，我觉得用着还是很不错的，最主要的是插件很多，下载也很方便

* 下载AutoJS的插件

  ![image-20201120203755387](http://cdn.fxhaoo.cn/image-20201120203755387.png)

* 开启AutoJS的服务

  * `ctrl`+ `shift` +  `P`打开命令面板

  * 输入 `auto.js：start server`，选择

    ![image-20201120204505007](http://cdn.fxhaoo.cn/image-20201120204505007.png)

#### 连接手机

* 手机安装 **Autojs.apk** 软件

* 为**Auto.js**开启必要权限

  ![image-20201120205246401](http://cdn.fxhaoo.cn/image-20201120205246401.png)

  * 稳定模式：之前试的时候没开这个检测时可以检测到很多控件信息，但需要点击的控件却没有点击属性，开了之后，检测的控件信息相对较少，但比较稳定。

  * 悬浮窗：反正就需要用

  * 连接电脑：**这个需要和PC端连接的同一个网络，同时PC端开启了Auto.JS服务**，然后输入PC的IP地址即可（可用在PC端cmd中用ipconfig来查看），当弹出下面的提示时，说明已经连接成功

    ![image-20201120205943014](http://cdn.fxhaoo.cn/image-20201120205943014.png)

#### 基础使用

* 这里可以参考官方文档来进行编写，里面对各个模块的各个接口如何使用和参数都有比较清楚的描述，自己摸着摸着还比较容易上手~

* 如在vscode新建一个js文件写入一段最牛批的代码 `alert('hello world')`，然后按下`F5`，这时就会在手机上执行

  ![image-20201121160111110](http://cdn.fxhaoo.cn/image-20201121160111110.png)

* ![image-20201123154825901](http://cdn.fxhaoo.cn/image-20201123154825901.png)

  我们可以通过控件分析来获取我们需要操作控件的属性，然后根据控件的属性来查找定位，这一块可以参考文档里的 [ WidgetsBaseAutomation](https://hyb1996.github.io/AutoJs-Docs/#/widgetsBasedAutomation) 模块来操作。

* 当我们编写完脚本的时候，可以在vscode中打开命令面板（`ctrl`+ `shift` +  `P`），输入Autojs，选择`Save On Device`保存到设备中，然后我们就可以在设备中单独运行脚本了。

### Demo

* 没错这次的**Demo**又是抢餐的脚本，不要问我为什么，问就是为了不挨饿~

* 具体的思路跟之前的大体相同，增加了一些比较人性化的功能，像抢餐份数的选择，餐品的选择，避免一通乱抢，浪费粮食。。（其实主要是怕被打~~）

* 执行的速度和方便性比之前好多了，如下图，从发现送餐连接，到点进去识别点击按钮，耗时在1s左右（还得看手机性能，这是我用模拟器的），基本是可以抢到~

  ![image-20201123163042553](http://file.fxhaoo.cn/image-20201123163042553.png)

* [脚本文件](http://file.fxhaoo.cn/food_v1.3.1.rar)

* 欢迎各位大佬提出BUG及建议，或者有什么好的思路或想法也可以交流学习~~

